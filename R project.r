install.packages("ff")
install.packages("sqldf")
install.packages("rpart")
install.packages("Amelia")
install.packages("class")                     
install.packages("caret")
install.packages("corrplot", dependencies = TRUE)
install.packages("e1071")
install.packages("truncnorm")
install.packages("grDevices")
install.packages("pca")
install.packages("neuralnet")
install.packages("devtools")
install.packages("ggplot2")
install.packages("ggvis")
install.packages("pROC")
install.packages("ROCR")   
install.packages("randomForest")
install.packages("caTools")

library(ff)
library(sqldf)
library(rpart)
library(corrplot)
library(Amelia)
library(class)
library(caret)
library(e1071)
library(truncnorm)
library(grDevices)
library(pca)
library(neuralnet)
library(devtools)
install_github("ggbiplot", "vqv")
library(ggbiplot)
library(ggplot2)
library(ggvis)
library(pROC)
library(ROCR)
library(randomForest)
library(caTools)

setwd('E:/Outbrain/')

#Load the tables into dataFrames 
events <- read.table(file="events.csv", nrows = 1000000, header = TRUE,sep = ",")
page_views_sample <- read.table(file="page_views_sample.csv", header=TRUE, nrows = 1000000, sep = ",")
clicks_train <- read.table(file="clicks_train.csv",nrows = 1000000, header=TRUE, sep =  ",")
clicks_test <- read.table(file="clicks_test.csv", header=TRUE, nrows = 1000000, sep = ",")
promoted_content <- read.table(file="promoted_content.csv", header=TRUE, nrows = 1000000, sep = ",")
document_entities <- read.table(file="documents_entities.csv", header=TRUE, nrows = 1000000, sep = ",")
document_topics <- read.table(file="documents_topics.csv", header=TRUE, nrows = 1000000, sep = ",")
document_categories <- read.table(file="documents_categories.csv", header=TRUE, nrows = 1000000, sep = ",")



#Click_test table
typeof(clicks_test$display_id)
boxplot.stats(clicks_test$display_id)
typeof(clicks_test$ad_id)
boxplot.stats(clicks_test$ad_id)

indx_na<- apply(clicks_test, 2, function(x) any(is.na(x)))
indx_na
indx_inf<- apply(clicks_test, 2, function(x) any(is.infinite(x)))
indx_inf
indx_nan<- apply(clicks_test, 2, function(x) any(is.nan(x)))
indx_nan
indx_null<- apply(clicks_test, 2, function(x) any(is.null(x)))
indx_null

#Events table--------------------------------------------------------
typeof(events$display_id)
boxplot.stats(events$display_id)

typeof(events$uuid)
#boxplot doesn't evaluate user_id variable because it isn't numeric/character.
boxplot.stats(events$uuid)

typeof(events$document_id)

boxplot.stats(events$document_id)

typeof(events$timestamp)
boxplot.stats(events$timestamp)

typeof(events$platform)
typeof(events$geo_location)

indx_na<- apply(events, 2, function(x) any(is.na(x)))
indx_na
indx_inf<- apply(events, 2, function(x) any(is.infinite(x)))
indx_inf
indx_nan<- apply(events, 2, function(x) any(is.nan(x)))
indx_nan
indx_null<- apply(events, 2, function(x) any(is.null(x)))
indx_null

#Clicks_train table

typeof(clicks_train$display_id)
boxplot.stats(clicks_train$display_id)

typeof(clicks_train$ad_id)
boxplot.stats(clicks_train$ad_id)

typeof(clicks_train$clicked)

indx_na<- apply(clicks_train, 2, function(x) any(is.na(x)))
indx_na
indx_inf<- apply(clicks_train, 2, function(x) any(is.infinite(x)))
indx_inf
indx_nan<- apply(clicks_train, 2, function(x) any(is.nan(x)))
indx_nan
indx_null<- apply(clicks_train, 2, function(x) any(is.null(x)))
indx_null



#Promoted_content table

typeof(promoted_content$ad_id)
boxplot.stats(promoted_content$ad_id)

typeof(promoted_content$document_id)

typeof(promoted_content$campaign_id)
boxplot.stats(promoted_content$ad_id)

typeof(promoted_content$advertiser_id)
boxplot.stats(promoted_content$advertiser_id)

indx_na<- apply(promoted_content, 2, function(x) any(is.na(x)))
indx_na
indx_inf<- apply(promoted_content, 2, function(x) any(is.infinite(x)))
indx_inf
indx_nan<- apply(promoted_content, 2, function(x) any(is.nan(x)))
indx_nan
indx_null<- apply(promoted_content, 2, function(x) any(is.null(x)))
indx_null


#Page_views_sample table

typeof(page_views_sample$uuid)
boxplot.stats(page_views_sample$uuid)

typeof(page_views_sample$document_id)
boxplot.stats(page_views_sample$document_id)

typeof(page_views_sample$timestamp)
boxplot.stats(page_views_sample$timestamp)

typeof(page_views_sample$platform)
boxplot.stats(page_views_sample$platform)

typeof(page_views_sample$geo_location)

typeof(page_views_sample$traffic_source)
boxplot.stats(page_views_sample$traffic_source)

indx_na<- apply(page_views_sample, 2, function(x) any(is.na(x)))
indx_na
indx_inf<- apply(page_views_sample, 2, function(x) any(is.infinite(x)))
indx_inf
indx_nan<- apply(page_views_sample, 2, function(x) any(is.nan(x)))
indx_nan
indx_null<- apply(page_views_sample, 2, function(x) any(is.null(x)))
indx_null


#Document_entities table

typeof(document_entities$document_id)
boxplot.stats(document_entities$document_id)

typeof(document_entities$entity_id)
boxplot.stats(document_entities$entity_id)

typeof(document_entities$confidence_level)
boxplot.stats(document_entities$confidence_level)

indx_na<- apply(document_entities, 2, function(x) any(is.na(x)))
indx_na
indx_inf<- apply(document_entities, 2, function(x) any(is.infinite(x)))
indx_inf
indx_nan<- apply(document_entities, 2, function(x) any(is.nan(x)))
indx_nan
indx_null<- apply(document_entities, 2, function(x) any(is.null(x)))
indx_null

#Document_topics table

typeof(document_topics$document_id)
boxplot.stats(document_topics$document_id)

typeof(document_topics$topic_id)
boxplot.stats(document_topics$topic_id)

typeof(document_topics$confidence_level)
boxplot(document_topics$confidence_level)
boxplot.stats(document_topics$confidence_level)

indx_na<- apply(document_topics, 2, function(x) any(is.na(x)))
indx_na
indx_inf<- apply(document_topics, 2, function(x) any(is.infinite(x)))
indx_inf
indx_nan<- apply(document_topics, 2, function(x) any(is.nan(x)))
indx_nan
indx_null<- apply(document_topics, 2, function(x) any(is.null(x)))
indx_null


#Document_categories table

typeof(document_categories$document_id)
boxplot.stats(document_categories$document_id)

typeof(document_categories$category_id)
boxplot.stats(document_categories$category_id)

typeof(document_categories$confidence_level)
boxplot.stats(document_categories$confidence_level)

indx_na<- apply(document_categories, 2, function(x) any(is.na(x)))
indx_na
indx_inf<- apply(document_categories, 2, function(x) any(is.infinite(x)))
indx_inf
indx_nan<- apply(document_categories, 2, function(x) any(is.nan(x)))
indx_nan
indx_null<- apply(document_categories, 2, function(x) any(is.null(x)))
indx_null


#missingness map test
missmap(page_views_sample,legend = TRUE,col = c("wheat","darkred"))


page_views_sample$platform<-(as.factor(page_views_sample$platform))
page_views_sample$traffic_source<-(as.factor(page_views_sample$traffic_source))

sample_m10<-model.matrix(~platform - 1,data =page_views_sample)
sample_m11<-model.matrix(~traffic_source - 1,data =page_views_sample)

page_views1<-cbind(page_views_sample,sample_m10)
page_views<-cbind(page_views1,sample_m11)

# check for NA's here: No NA values found
events_pageviews_na_val <- sqldf("select * from page_views as b")
View(events_pageviews_na_val)

events_pageviews_na <- sqldf("select a.display_id,a.uuid,a.document_id,b.geo_location,b.platform1,b.platform2,b.platform3,b.traffic_source1,b.traffic_source2,b.traffic_source3 from events as a,page_views as b where a.uuid = b.uuid and b.geo_location ='NA'")
View(events_pageviews_na)


#Joining `events` and `page_views`
events_pageviews <- sqldf("select a.display_id,a.uuid,a.document_id,a.geo_location,b.platform1,b.platform2,b.platform3,b.traffic_source1,b.traffic_source2,b.traffic_source3 from events as a,page_views as b where a.uuid = b.uuid")
